# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import (ModelSQL, ModelView, fields, Workflow, Model,
    DeactivableMixin)
from trytond.model import Unique
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, If, Bool
from trytond.transaction import Transaction
from trytond.wizard import (Wizard, StateAction, StateTransition,
    StateView, Button)
from trytond.tools import reduce_ids
from sql import Literal
from trytond.i18n import gettext
from trytond.exceptions import UserError

STATES = [
    ('draft', 'Draft'),
    ('waiting', 'Waiting'),
    ('done', 'Done'),
    ('confirmed', 'Confirmed'),
    ('cancelled', 'Cancelled')
]


class SaleClaimType(DeactivableMixin, ModelSQL, ModelView):
    """Sale claim type"""
    __name__ = 'sale.claim.type'

    name = fields.Char('Name', required=True,
                       states={'readonly': ~Eval('active')},
                       depends=['active'])
    code = fields.Char('Code',
                       states={'readonly': ~Eval('active')},
                       depends=['active'])
    severity_ = fields.Selection(
            [('incidence', 'Incidence'),
             ('claim', 'Claim')], 'Severity', required=True,
            states={'readonly': ~Eval('active')},
            depends=['active'])

    def get_rec_name(self, name):
        if self.code:
            return '[%s] %s' % (self.code, self.name)
        return self.name

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                ('name', ) + tuple(clause[1:]),
                ('code', ) + tuple(clause[1:])]

    @staticmethod
    def default_severity_():
        return 'claim'


class SaleClaim(Workflow, ModelSQL, ModelView):
    """Sale claim"""
    __name__ = 'sale.claim'

    sale = fields.Many2One('sale.sale', 'Sale', required=True, select=True,
        states={'readonly': (Eval('state') != 'draft') | (Eval('id', 0) > 0)},
        depends=['state', 'id'])
    party = fields.Function(
        fields.Many2One('party.party', 'Party'),
        'get_sale_field', searcher='search_sale_field')
    sale_date = fields.Function(fields.Date('Sale date'),
        'get_sale_field', searcher='search_sale_field')
    shipment = fields.Function(
        fields.Many2One('stock.shipment.out', 'Shipment'),
        'get_shipment', searcher='search_shipment')
    allowed_products = fields.Function(
        fields.Many2Many('product.product', None, None, 'Products'),
        'on_change_with_allowed_products')
    currency = fields.Function(
            fields.Many2One('currency.currency', 'Currency'), 'get_sale_field')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
                                      'get_sale_field')
    state = fields.Selection(STATES, 'State',
                            readonly=True, required=True)
    lines = fields.One2Many('sale.claim.line', 'claim', 'Lines',
                            states={'readonly': (Eval('state') != 'draft')},
                            depends=['state'])

    @classmethod
    def __setup__(cls):
        super(SaleClaim, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('sale_uk1', Unique(t, t.sale),
             'sale_claim.msg_sale_claim_sale_uk1')
        ]
        cls._transitions |= set((('draft', 'waiting'),
                                 ('waiting', 'confirmed'),
                                 ('waiting', 'draft'),
                                 ('confirmed', 'waiting'),
                                 ('confirmed', 'done'),
                                 ('draft', 'cancelled'),
                                 ('cancelled', 'draft'),
                                 ('done', 'draft')))
        cls._buttons.update({
            'cancel': {
                'invisible': Eval('state').in_(['cancelled', 'waiting', 'done']),
                'depends': ['state']
                },
            'draft': {
                'invisible': Eval('state').in_(['draft', 'confirmed']),
                'icon': If(Eval('state') == 'cancelled',
                    'tryton-undo', 'tryton-back'),
                'depends': ['state']
                },
            'wait': {
                'invisible': ~Eval('state').in_(['draft', 'confirmed']),
                'icon': If(Eval('state') == 'confirmed',
                    'tryton-back', 'tryton-forward'),
                'depends': ['state']
                },
            'confirm': {
                'invisible': Eval('state') != 'waiting',
                'icon': 'tryton-forward',
                'depends': ['state']
                },
            'do': {
                'invisible': Eval('state') != 'confirmed',
                'depends': ['state']
                },
        })

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()

        super().__register__(module_name)

        # Migration from 5.6: rename state cancel to cancelled
        cursor.execute(*sql_table.update(
                [sql_table.state], ['cancelled'],
                where=sql_table.state == 'cancel'))

    def get_rec_name(self, name):
        return self.sale.rec_name

    @classmethod
    def default_state(cls):
        return 'draft'

    def get_sale_field(self, name=None):
        res = getattr(self.sale, name, None)
        if isinstance(res, Model):
            return res.id
        return res

    @classmethod
    def search_sale_field(cls, name, clause):
        return [('sale.%s' % clause[0], ) + tuple(clause[1:])]

    def get_shipment(self, name=None):
        if self.sale and self.sale.shipments:
            return self.sale.shipments[0].id
        return None

    @classmethod
    def search_shipment(cls, name, clause):
        return [('sale.lines.moves.shipment.number', ) + tuple(clause[1:]) +
                ('stock.shipment.out', )]

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                [('sale', ) + tuple(clause[1:])],
                [('shipment', ) + tuple(clause[1:])]]

    @fields.depends('sale', '_parent_sale.shipments')
    def on_change_sale(self):
        if self.sale:
            self.party = self.sale.party
            self.sale_date = self.sale.sale_date
            if self.sale.shipments:
                self.shipment = self.sale.shipments[0]
            self.currency = self.sale.currency
            self.currency_digits = self.sale.currency_digits

    @classmethod
    def delete(cls, records):
        cls.cancel(records)
        for record in records:
            if record.state != 'cancelled':
                raise UserError(gettext(
                    'sale_claim.msg_sale_claim_delete_cancel',
                    sale_claim=record.rec_name))
        super(SaleClaim, cls).delete(records)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        User = pool.get('res.user')
        Group = pool.get('res.group')

        def in_group():
            group = Group(ModelData.get_id('sale_claim',
                'group_sale_claim_admin'))
            transaction = Transaction()
            user_id = transaction.user
            if user_id == 0:
                user_id = transaction.context.get('user', user_id)
            if user_id == 0:
                return True
            user = User(user_id)
            return group in user.groups

        done_records = [r for r in records if r.state == 'done']

        if done_records and not in_group():
            raise UserError(gettext(
                'sale_claim.msg_sale_claim_draft_done'))

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting')
    def wait(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, records):
        cls.check_amount(records)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def do(cls, records):
        cls.check_amount(records)

    @fields.depends('sale', '_parent_sale.lines')
    def on_change_with_allowed_products(self, name=None):
        if self.sale and self.sale.lines:
            return [l.product.id for l in self.sale.lines if l.type == 'line']
        return []

    @classmethod
    def check_amount(cls, records):
        for record in records:
            if any(l.amount is None for l in record.lines):
                raise UserError(gettext(
                    'sale_claim.msg_sale_claim_missing_amount',
                    sale_claim=record.rec_name))


class SaleClaimLine(ModelSQL, ModelView):
    """Sale claim line"""
    __name__ = 'sale.claim.line'

    claim = fields.Many2One('sale.claim', 'Claim', required=True,
        ondelete='CASCADE')
    product = fields.Many2One('product.product', 'Product', required=True,
        domain=[
            If(Bool(Eval('sale_line')),
                ('id', 'in', Eval('allowed_products', [])),
                ()),
            ('id', 'in', Eval('_parent_claim', {}).get('allowed_products'))],
        states={
            'readonly': Eval('claim_state') != 'draft',
        },
        depends=['sale_line', 'allowed_products', 'claim_state'])
    type_ = fields.Many2One('sale.claim.type', 'Type', required=True,
        states={
            'readonly': Eval('claim_state') != 'draft',
        },
        depends=['claim_state'])
    amount = fields.Numeric('Amount',
        digits=(16, Eval('_parent_claim', {}).get('currency_digits', 2)),
        states={
            'required': Eval('claim_state') == 'done',
            'readonly': Eval('claim_state').in_(['cancelled', 'done']),
        },
        depends=['claim_state'])
    note = fields.Text('Note', states={
            'readonly': Eval('claim_state') == 'done',
        },
        depends=['claim_state'])
    sale_line = fields.Many2One('sale.line', 'Sale Line', select=True,
        domain=[
            ('type', '=', 'line'),
            ('sale', '=', Eval('sale')),
            ('product', '!=', None)
        ],
        states={
            'readonly': Eval('claim_state') != 'draft',
        },
        depends=['sale', 'claim_state'])
    sale = fields.Function(fields.Many2One('sale.sale', 'Sale'),
        'on_change_with_sale')
    allowed_products = fields.Function(
        fields.Many2Many('product.product', None, None, 'Products'),
        'on_change_with_allowed_products')
    claim_state = fields.Function(
        fields.Selection('get_claim_states', 'Claim state'),
        'on_change_with_claim_state')

    @fields.depends('claim', '_parent_claim.sale')
    def on_change_with_sale(self, name=None):
        if self.claim and self.claim.sale:
            return self.claim.sale.id
        return None

    @fields.depends('sale_line')
    def on_change_sale_line(self):
        self.product = None
        if self.sale_line:
            products = self.on_change_with_allowed_products()
            if len(products) == 1:
                self.product = products[0]

    @fields.depends('sale_line')
    def on_change_with_allowed_products(self, name=None):
        if self.sale_line:
            return [self.sale_line.product.id]
        return []

    @classmethod
    def get_claim_states(cls):
        pool = Pool()
        Claim = pool.get('sale.claim')
        return Claim.fields_get(['state'])['state']['selection']

    @fields.depends('claim', '_parent_claim.state')
    def on_change_with_claim_state(self, name=None):
        if self.claim:
            return self.claim.state

    @classmethod
    def delete(cls, records):
        for record in records:
            if record.claim.state in {'waiting', 'confirmed', 'done'}:
                raise UserError(gettext(
                    'sale_claim.msg_sale_claim_delete_cancel',
                    sale_claim=record.claim.rec_name))
        super().delete(records)


class ClaimSale(Wizard):
    """Claim sale"""
    __name__ = 'sale.claim_create'

    start = StateTransition()
    edit_ = StateView('sale.claim',
                      'sale_claim.sale_claim_view_form',
                      [Button('Cancel', 'end', 'tryton-cancel'),
                       Button('Ok', 'open_', 'tryton-ok', default=True)])
    open_ = StateAction('sale_claim.act_sale_claim')

    def transition_start(self):
        pool = Pool()
        Claim = pool.get('sale.claim')

        sale_id = Transaction().context.get('active_id')
        claim = Claim.search_read([
            ('sale', '=', sale_id)], fields_names=['id'], limit=1)
        if not claim:
            return 'edit_'
        return 'open_'

    def default_edit_(self, fields):
        return {'sale': Transaction().context['active_id']}

    def do_open_(self, action):
        pool = Pool()
        Claim = pool.get('sale.claim')

        sale_id = Transaction().context.get('active_id')
        claim = Claim.search_read([
            ('sale', '=', sale_id)], fields_names=['id'], limit=1)
        if not claim:
            self.edit_.save()
            claim = [{'id': self.edit_.id}]
        claim, = claim

        data = {'res_id': [claim['id']]}
        action['views'].reverse()
        return action, data


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    claims = fields.One2Many('sale.claim', 'sale', 'Claims', readonly=True)
    claimed = fields.Function(fields.Boolean('Claimed'),
        'get_claimed', searcher='search_claimed')

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('claims', None)
        return super().copy(records, default=default)

    @classmethod
    def get_claimed(cls, records, name):
        pool = Pool()
        Claim = pool.get('sale.claim')
        claim = Claim.__table__()

        res = {r.id: False for r in records}
        cursor = Transaction().connection.cursor()
        query = claim.select(claim.sale, Literal(True),
            where=reduce_ids(claim.sale, records),
            group_by=claim.sale)
        cursor.execute(*query)

        res.update(dict(cursor.fetchall()))
        return res

    @classmethod
    def search_claimed(cls, name, clause):
        reverse = {
            '=': '!=',
            '!=': '=',
            }
        if clause[1] in reverse:
            if not clause[2]:
                return [('claims', clause[1], None)]
            else:
                return [('claims', reverse[clause[1]], None)]
        else:
            return []
