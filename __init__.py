# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .sale_claim import (SaleClaimType, SaleClaim, SaleClaimLine,
                         ClaimSale, Sale)


def register():
    Pool.register(
        SaleClaimType,
        SaleClaim,
        SaleClaimLine,
        Sale,
        module='sale_claim', type_='model')
    Pool.register(
        ClaimSale,
        module='sale_claim', type_='wizard')